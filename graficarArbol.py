import matplotlib.pyplot as plt
import networkx as nx

class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.value)

def create_tree():
    root = Node('A')
    root.left = Node('B')
    root.right = Node('C')
    root.left.left = Node('D')
    root.left.right = Node('E')
    root.right.left = Node('F')
    root.right.right = Node('G')
    
    return root

root = create_tree()
G = nx.Graph()

def add_nodes_edges(node, parent=None):
    G.add_node(node.value)
    if parent:
        G.add_edge(parent.value, node.value)
    if node.left:
        add_nodes_edges(node.left, node)
    if node.right:
        add_nodes_edges(node.right, node)

add_nodes_edges(root)
pos = nx.drawing.layout.kamada_kawai_layout(G)
nx.draw(G, pos, with_labels=True, node_size=800, node_color="lightblue", font_size=20, font_weight="bold", width=2, edge_color="grey")
plt.show()
